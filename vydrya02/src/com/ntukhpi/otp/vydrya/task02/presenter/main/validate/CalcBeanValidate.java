package com.ntukhpi.otp.vydrya.task02.presenter.main.validate;

import com.ntukhpi.otp.vydrya.task02.R;
import com.ntukhpi.otp.vydrya.task02.presenter.validate.Validate;
import com.ntukhpi.otp.vydrya.task02.utils.constant.ErrorConstant;
import com.ntukhpi.otp.vydrya.task02.view.bean.CalcBean;
import com.ntukhpi.otp.vydrya.task02.view.bean.error.ErrorHolder;

import android.util.Log;

public class CalcBeanValidate implements Validate<ErrorHolder<Integer, String>, CalcBean> {
	private static final String TAG = CalcBeanValidate.class.getName();

	@Override
	public ErrorHolder<Integer, String> validate(CalcBean e) {
		Log.d(TAG, "validate start");
		ErrorHolder<Integer, String> error = new ErrorHolder<>();
		if (e.getParamOne().trim().isEmpty()) {
			error.add(R.id.param_one, ErrorConstant.ERROR_EMPTY);
		}
		if (e.getParamTwo().trim().isEmpty()) {
			error.add(R.id.param_two, ErrorConstant.ERROR_EMPTY);
		}
		Log.d(TAG, "validate done");
		return error;
	}
}
