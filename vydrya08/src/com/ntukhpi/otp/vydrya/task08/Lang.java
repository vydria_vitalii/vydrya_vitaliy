package com.ntukhpi.otp.vydrya.task08;

public enum Lang {
	RU("ru"), EN("en");
	private final String value;

	private Lang(final String lang) {
		value = lang;
	}

	public String value() {
		return value;
	}
}
