package com.ntukhpi.otp.vydrya.task02.utils.constant;

public final class ErrorConstant {
	private ErrorConstant() {
		throw new UnsupportedOperationException("non instance ErrorConstant");
	}

	public static final String ERROR_EMPTY = "The item cannot be empty";
}
