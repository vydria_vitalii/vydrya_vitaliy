package com.ntukhpi.otp.vydrya.task07;

import java.io.Closeable;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.database.sqlite.SQLiteOpenHelper;

//http://developer.alexanderklimov.ru/android/sqlite/android-sqlite.php
//guides.codepath.com/android/local-databases-with-sqliteopenhelper
//http://www.theappguruz.com/blog/android-using-sqlite-database
public class DBManager extends SQLiteOpenHelper implements Closeable {
	private static final String TAG = DBManager.class.getName();

	public static final String DATABASE_NAME = "carstore.db"; // название бд
	public static final int DATABASE_VERSION = 1;

	private static final String[][] DATA_DB = { { "BMW f10", "Выдря Виталий" }, { "Porsche 911", "Домнин Феликc" },
			{ "Audi A6", "Валера Валера" }, { "Lamborghini Gallardo Cabrio", "Павел Дуров" },
			{ "Volkswagen Polo", "Павел Афонов" }, { "Toyota Camry", "Алла Новикова" },
			{ "Toyota RAV-4", "Валентин Трофимов" }, { "Kia Sportage", "Дарья Шепелева" } };

	public DBManager(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.d(TAG, "onCreate start");
		final String createQuary = "CREATE TABLE " + CarUtils.NAME_TABLE + "( " + CarUtils.ID_CAR
				+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + CarUtils.CAR_NAME + " TEXT, " + CarUtils.OWNER + " TEXT);";
		db.execSQL(createQuary);
		ContentValues cv = new ContentValues();
		for (int i = 0; i < DATA_DB.length; i++) {
			cv.put(CarUtils.CAR_NAME, DATA_DB[i][0]);
			cv.put(CarUtils.OWNER, DATA_DB[i][1]);
			db.insert(CarUtils.NAME_TABLE, null, cv);
			cv.clear();
		}
		Log.d(TAG, "onCreate done");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.d(TAG, "onUpgrade start");
		final String updatQuery = "DROP TABFLE IF EXISTS " + CarUtils.NAME_TABLE;
		db.execSQL(updatQuery);
		onCreate(db);
		Log.d(TAG, "o nUpgrade done");
	}
}
