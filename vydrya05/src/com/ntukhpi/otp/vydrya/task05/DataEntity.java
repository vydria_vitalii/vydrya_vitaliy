package com.ntukhpi.otp.vydrya.task05;

import java.io.Serializable;


public class DataEntity implements Serializable {
	private static final long serialVersionUID = 3222848383027561058L;
	private String name1;
	private String name2;
	private int img;
	private boolean select;

	/**
	 * @return the select
	 */
	public boolean isSelect() {
		return select;
	}

	/**
	 * @param select the select to set
	 */
	public void setSelect(boolean select) {
		this.select = select;
	}

	public DataEntity() {
	}

	/**
	 * @return the name1
	 */
	public String getName1() {
		return name1;
	}

	/**
	 * @param name1
	 *            the name1 to set
	 */
	public void setName1(String name1) {
		this.name1 = name1;
	}

	/**
	 * @return the name2
	 */
	public String getName2() {
		return name2;
	}

	/**
	 * @param name2
	 *            the name2 to set
	 */
	public void setName2(String name2) {
		this.name2 = name2;
	}

	/**
	 * @return the img
	 */
	public int getImg() {
		return img;
	}

	/**
	 * @param img
	 *            the img to set
	 */
	public void setImg(int img) {
		this.img = img;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + img;
		result = prime * result + ((name1 == null) ? 0 : name1.hashCode());
		result = prime * result + ((name2 == null) ? 0 : name2.hashCode());
		result = prime * result + (select ? 1231 : 1237);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DataEntity)) {
			return false;
		}
		DataEntity other = (DataEntity) obj;
		if (img != other.img) {
			return false;
		}
		if (name1 == null) {
			if (other.name1 != null) {
				return false;
			}
		} else if (!name1.equals(other.name1)) {
			return false;
		}
		if (name2 == null) {
			if (other.name2 != null) {
				return false;
			}
		} else if (!name2.equals(other.name2)) {
			return false;
		}
		if (select != other.select) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DataEntity [");
		if (name1 != null) {
			builder.append("name1=");
			builder.append(name1);
			builder.append(", ");
		}
		if (name2 != null) {
			builder.append("name2=");
			builder.append(name2);
			builder.append(", ");
		}
		builder.append("img=");
		builder.append(img);
		builder.append(", select=");
		builder.append(select);
		builder.append("]");
		return builder.toString();
	}
}
