package com.ntukhpi.otp.vydrya.task02.view.activity.main.extractor;

import com.ntukhpi.otp.vydrya.task02.view.activity.main.MainActivity;
import com.ntukhpi.otp.vydrya.task02.view.bean.CalcBean;
import com.ntukhpi.otp.vydrya.task02.view.extractor.Extractor;

public class CalcBeanExtractor implements Extractor<CalcBean, MainActivity> {

	@Override
	public CalcBean extract(MainActivity activity) {
		CalcBean calcElement = new CalcBean();
		calcElement.setParamOne(activity.getParamOne().getText().toString());
		calcElement.setParamTwo(activity.getParamTwo().getText().toString());
		calcElement.setOperation(activity.getOperation().getSelectedItem().toString());
		return calcElement;
	}
}
