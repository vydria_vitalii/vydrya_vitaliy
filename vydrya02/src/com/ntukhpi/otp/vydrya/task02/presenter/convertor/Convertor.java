package com.ntukhpi.otp.vydrya.task02.presenter.convertor;

public interface Convertor<E, T> {
	T convert(E e);
}
