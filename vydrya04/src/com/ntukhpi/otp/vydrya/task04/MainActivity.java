package com.ntukhpi.otp.vydrya.task04;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {
	private static final String TAG = MainActivity.class.getName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate start");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		final Button button = (Button) findViewById(R.id.button_bonus);
		button.setOnClickListener(this);
		Log.d(TAG, "onCreate done");		
	}

	@Override
	public void onClick(View v) {
		Log.d(TAG, "onClick start");
		setContentView(R.layout.bonus);
		Log.d(TAG, "onClick done");
	}
}
