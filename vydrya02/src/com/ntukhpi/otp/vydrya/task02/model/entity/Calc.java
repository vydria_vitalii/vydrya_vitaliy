package com.ntukhpi.otp.vydrya.task02.model.entity;

/**
 * @author vitaliy
 *
 */
public class Calc implements Entity {
	private static final long serialVersionUID = 417286964893350365L;
	private Double paramOne;
	private Double paramTwo;
	private Operations operations;
	private Double reulst;

	public Calc() {
	}

	/**
	 * @return the paramOne
	 */
	public Double getParamOne() {
		return paramOne;
	}

	/**
	 * @param paramOne the paramOne to set
	 */
	public void setParamOne(Double paramOne) {
		this.paramOne = paramOne;
	}

	/**
	 * @return the paramTwo
	 */
	public Double getParamTwo() {
		return paramTwo;
	}

	/**
	 * @param paramTwo the paramTwo to set
	 */
	public void setParamTwo(Double paramTwo) {
		this.paramTwo = paramTwo;
	}

	/**
	 * @return the operations
	 */
	public Operations getOperations() {
		return operations;
	}

	/**
	 * @param operations the operations to set
	 */
	public void setOperations(Operations operations) {
		this.operations = operations;
	}

	/**
	 * @return the reulst
	 */
	public Double getReulst() {
		return reulst;
	}

	/**
	 * @param reulst the reulst to set
	 */
	public void setReulst(Double reulst) {
		this.reulst = reulst;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((operations == null) ? 0 : operations.hashCode());
		result = prime * result + ((paramOne == null) ? 0 : paramOne.hashCode());
		result = prime * result + ((paramTwo == null) ? 0 : paramTwo.hashCode());
		result = prime * result + ((reulst == null) ? 0 : reulst.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Calc)) {
			return false;
		}
		Calc other = (Calc) obj;
		if (operations != other.operations) {
			return false;
		}
		if (paramOne == null) {
			if (other.paramOne != null) {
				return false;
			}
		} else if (!paramOne.equals(other.paramOne)) {
			return false;
		}
		if (paramTwo == null) {
			if (other.paramTwo != null) {
				return false;
			}
		} else if (!paramTwo.equals(other.paramTwo)) {
			return false;
		}
		if (reulst == null) {
			if (other.reulst != null) {
				return false;
			}
		} else if (!reulst.equals(other.reulst)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Calc [");
		if (paramOne != null) {
			builder.append("paramOne=");
			builder.append(paramOne);
			builder.append(", ");
		}
		if (paramTwo != null) {
			builder.append("paramTwo=");
			builder.append(paramTwo);
			builder.append(", ");
		}
		if (operations != null) {
			builder.append("operations=");
			builder.append(operations);
			builder.append(", ");
		}
		if (reulst != null) {
			builder.append("reulst=");
			builder.append(reulst);
		}
		builder.append("]");
		return builder.toString();
	}
}