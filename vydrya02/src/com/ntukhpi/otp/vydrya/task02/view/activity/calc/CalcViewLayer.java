package com.ntukhpi.otp.vydrya.task02.view.activity.calc;

import com.ntukhpi.otp.vydrya.task02.model.entity.Calc;
import com.ntukhpi.otp.vydrya.task02.view.ViewLayer;

public interface CalcViewLayer extends ViewLayer {
	void returnToBackActivity(Calc calc);
}
