package com.ntukhpi.otp.vydrya.task02.view;

import java.util.Map;

import com.ntukhpi.otp.vydrya.task02.view.bean.error.ErrorHolder;
import com.ntukhpi.otp.vydrya.task02.view.bean.error.ErrorMessage;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.TextView;

public abstract class AbstractViewLayer extends Activity implements ViewLayer {
	private static final String OK = "OK";
	private static final String TAG = AbstractViewLayer.class.getName();

	/**
	 * Default constructor.
	 */
	public AbstractViewLayer() {
		Log.d(TAG, "AbstractViewLayer start");
		Log.d(TAG, "AbstractViewLayer done");
	}

	protected abstract Activity getActivity();

	public void showInvalidData(ErrorHolder<Integer, String> error) {
		Log.d(TAG, "showInvalidData start");
		for(Map.Entry<Integer, String> entry : error.getErrors().entrySet()) {
			final TextView view = (TextView) getActivity().findViewById(entry.getKey());
			view.setError(entry.getValue());
		}
		Log.d(TAG, "showInvalidData done");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ntukhpi.otp.vydrya.task02.view.ViewLayer#showError(java.util.logging.
	 * ErrorManager)
	 */
	@Override
	public void showError(final ErrorMessage error) {
		Log.d(TAG, "showError start");
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity().getApplicationContext());
		builder.setTitle(error.getTitle()).setMessage(error.getMessage()).setCancelable(false).setNegativeButton(OK,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						Log.d(TAG, "onClick start");
						dialog.cancel();
						final int pid = android.os.Process.myPid();
						android.os.Process.killProcess(pid);
						Log.d(TAG, "onClick done");
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
		Log.d(TAG, "showError done");
	}
}