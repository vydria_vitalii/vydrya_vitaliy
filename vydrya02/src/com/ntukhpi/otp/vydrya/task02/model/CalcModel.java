package com.ntukhpi.otp.vydrya.task02.model;

import com.ntukhpi.otp.vydrya.task02.model.entity.Calc;

public interface CalcModel {
	Calc calculation(Calc calc);
}
