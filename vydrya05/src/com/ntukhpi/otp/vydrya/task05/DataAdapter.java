package com.ntukhpi.otp.vydrya.task05;

import java.util.Comparator;

import android.widget.ListAdapter;
import android.widget.SpinnerAdapter;

public interface DataAdapter<T> extends ListAdapter, SpinnerAdapter {
	void add(T obj);

	void remove(int position);

	void sort(Comparator<T> comparator);
}
