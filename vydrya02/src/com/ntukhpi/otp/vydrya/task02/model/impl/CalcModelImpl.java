package com.ntukhpi.otp.vydrya.task02.model.impl;

import com.ntukhpi.otp.vydrya.task02.model.CalcModel;
import com.ntukhpi.otp.vydrya.task02.model.entity.Calc;

import android.util.Log;

public class CalcModelImpl implements CalcModel {
	private static final String TAG = CalcModelImpl.class.getName();

	@Override
	public Calc calculation(final Calc calc) {
		Log.d(TAG, "CalcServiceImpl start");
		switch (calc.getOperations()) {
		case ADDITION:
			calc.setReulst(calc.getParamOne() + calc.getParamTwo());
			break;
		case MULTIPLICATION:
			calc.setReulst(calc.getParamOne() * calc.getParamTwo());
			break;
		}
		Log.i(TAG, "result calculation: " + calc);
		Log.d(TAG, "CalcServiceImpl done");
		return calc;
	}
}
