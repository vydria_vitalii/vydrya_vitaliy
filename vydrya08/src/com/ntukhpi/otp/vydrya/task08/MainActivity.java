package com.ntukhpi.otp.vydrya.task08;

import java.util.Locale;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
	private static final String TAG = MainActivity.class.getName();
	private Button buttonRu;
	private Button buttonEn;
	private View mainView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate start");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		buttonEn = (Button) findViewById(R.id.button1);
		buttonRu = (Button) findViewById(R.id.button2);
		mainView = findViewById(R.id.main);

		buttonRu.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				setLocale(Lang.RU);
			}
		});

		buttonEn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				setLocale(Lang.EN);
			}
		});
		Log.d(TAG, "onCreate done");
	}

	@SuppressWarnings("unused")
	private void updateLocale() {
		Log.d(TAG, "updateLocale start");
		mainView.setBackgroundColor(getResources().getColor(R.color.background));
		Log.d(TAG, "updateLocale done");

	}

	private void setLocale(final Lang lang) {
		Log.d(TAG, "setLocale start");
		Configuration config = getBaseContext().getResources().getConfiguration();
		Locale locale = new Locale(lang.value());
		Locale.setDefault(locale);
		config.locale = locale;
		getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		updateLocale();
		Log.d(TAG, "setLocale done");
	}
}
