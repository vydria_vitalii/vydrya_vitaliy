package com.ntukhpi.otp.vydrya.task02.presenter.main.action;

import com.ntukhpi.otp.vydrya.task02.model.entity.Calc;
import com.ntukhpi.otp.vydrya.task02.presenter.action.ActionSupport;
import com.ntukhpi.otp.vydrya.task02.presenter.convertor.Convertor;
import com.ntukhpi.otp.vydrya.task02.presenter.validate.Validate;
import com.ntukhpi.otp.vydrya.task02.view.activity.main.MainViewLayer;
import com.ntukhpi.otp.vydrya.task02.view.bean.CalcBean;
import com.ntukhpi.otp.vydrya.task02.view.bean.error.ErrorHolder;

import android.util.Log;

public class MainActionButtonClick extends ActionSupport<CalcBean, Calc, MainViewLayer> {
	private static final String TAG = MainActionButtonClick.class.getName();
	private final Validate<ErrorHolder<Integer, String>, CalcBean> validate;
	private final Convertor<CalcBean, Calc> convertor;

	public MainActionButtonClick(Validate<ErrorHolder<Integer, String>, CalcBean> validate,
			Convertor<CalcBean, Calc> convertor) {
		super();
		this.validate = validate;
		this.convertor = convertor;
	}

	@Override
	public Calc convert(CalcBean e) {
		Log.d(TAG, "onClick start");
		Log.d(TAG, "onClick done");
		return this.convertor.convert(e);
	}

	@Override
	public void execute(MainViewLayer view, Calc e) {
		Log.d(TAG, "execute start");
		view.calculation(e);
		Log.d(TAG, "execute done");
	}

	@Override
	public void onValidationFailed(ErrorHolder<Integer, String> errorHolder, MainViewLayer view) {
		Log.d(TAG, "onValidationFailed start");
		view.showInvalidData(errorHolder);
		Log.d(TAG, "onValidationFailed done");
	}

	@Override
	public ErrorHolder<Integer, String> validate(CalcBean e) {
		Log.d(TAG, "validate start");
		final ErrorHolder<Integer, String> error = validate.validate(e);
		Log.i(TAG, "error: " + error);
		Log.d(TAG, "validate done");
		return error;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ntukhpi.otp.vydrya.task02.action.BaseAction#getBean(com.ntukhpi.otp.
	 * vydrya.task02.view.ViewLayer)
	 */
	@Override
	protected CalcBean getBean(MainViewLayer view) {
		Log.d(TAG, "getBean start");
		final CalcBean calcBean = view.getData();
		Log.i(TAG, "getBean: " + calcBean);
		Log.d(TAG, "getBean done");
		return calcBean;
	}

}
