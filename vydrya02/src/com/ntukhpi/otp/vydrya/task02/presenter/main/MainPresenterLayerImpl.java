package com.ntukhpi.otp.vydrya.task02.presenter.main;

import com.ntukhpi.otp.vydrya.task02.model.entity.Calc;
import com.ntukhpi.otp.vydrya.task02.presenter.action.Action;
import com.ntukhpi.otp.vydrya.task02.presenter.convertor.Convertor;
import com.ntukhpi.otp.vydrya.task02.presenter.main.action.MainActionButtonClick;
import com.ntukhpi.otp.vydrya.task02.presenter.main.convertor.CalcBeanToCalcConverter;
import com.ntukhpi.otp.vydrya.task02.presenter.main.validate.CalcBeanValidate;
import com.ntukhpi.otp.vydrya.task02.presenter.validate.Validate;
import com.ntukhpi.otp.vydrya.task02.view.activity.main.MainViewLayer;
import com.ntukhpi.otp.vydrya.task02.view.bean.CalcBean;
import com.ntukhpi.otp.vydrya.task02.view.bean.error.ErrorHolder;

import android.util.Log;

public class MainPresenterLayerImpl implements MainPresenterLayer {
	private static final String TAG = MainPresenterLayerImpl.class.getName();

	private final MainViewLayer view;
	private final Action<MainViewLayer> actionButtonClick;
	private final Validate<ErrorHolder<Integer, String>, CalcBean> validate;
	private final Convertor<CalcBean, Calc> convertor;

	public MainPresenterLayerImpl(final MainViewLayer view) {
		this.view = view;
		convertor = new CalcBeanToCalcConverter();
		validate = new CalcBeanValidate();
		actionButtonClick = new MainActionButtonClick(validate, convertor);
	}

	@Override
	public void onCalcButtonClick() {
		Log.d(TAG, "onCalcButtonClick start");
		actionButtonClick.execute(view);
		Log.d(TAG, "onCalcButtonClick done");
	}

	@Override
	public void onResultCalc(final Calc calc) {
		Log.d(TAG, "onResultCalc start");
		view.showResultCalc(calc);
		Log.d(TAG, "onResultCalc done");
	}

}
