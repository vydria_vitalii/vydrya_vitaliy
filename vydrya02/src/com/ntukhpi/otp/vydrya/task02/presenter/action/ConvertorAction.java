package com.ntukhpi.otp.vydrya.task02.presenter.action;

import com.ntukhpi.otp.vydrya.task02.model.entity.Entity;
import com.ntukhpi.otp.vydrya.task02.view.ViewLayer;

public interface ConvertorAction<E extends Entity, V extends ViewLayer> {
	void execute(V view, E e);
}
