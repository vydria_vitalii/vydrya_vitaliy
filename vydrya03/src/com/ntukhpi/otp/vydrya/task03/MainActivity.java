package com.ntukhpi.otp.vydrya.task03;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {
	private static final String TAG = MainActivity.class.getName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate start");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		final String userName = getResources().getString(R.string.user_name);
		final Button button = (Button) findViewById(R.id.button_show);

		button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Log.d(TAG, "onClick start");
            	Toast.makeText(getApplicationContext(), userName ,
            			 Toast.LENGTH_SHORT).show();
            	Log.d(TAG, "onClick done");
            }
        });

		Log.d(TAG, "onCreate done");
	}

	@Override
	protected void onStart() {
		Log.d(TAG, "onStart start");
		super.onStart();
		Log.d(TAG, "onStart done");
	}

	@Override
	protected void onResume() {
		Log.d(TAG, "onResume start");
		super.onResume();
		Log.d(TAG, "onResume done");
	}

	@Override
	protected void onRestart() {
		Log.d(TAG, "onRestart start");
		super.onRestart();
		Log.d(TAG, "onRestart done");
	}

	@Override
	protected void onPause() {
		Log.d(TAG, "onPause start");
		super.onPause();
		Log.d(TAG, "onPause done");
	}

	@Override
	protected void onStop() {
		Log.d(TAG, "onStop start");
		super.onStop();
		Log.d(TAG, "onStop done");
	}

	@Override
	protected void onDestroy() {
		Log.d(TAG, "onDestroy start");
		super.onDestroy();
		Log.d(TAG, "onDestroy done");
	}
}
