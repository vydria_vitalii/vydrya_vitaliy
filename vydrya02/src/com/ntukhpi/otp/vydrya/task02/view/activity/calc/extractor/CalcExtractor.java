package com.ntukhpi.otp.vydrya.task02.view.activity.calc.extractor;

import com.ntukhpi.otp.vydrya.task02.model.entity.Calc;
import com.ntukhpi.otp.vydrya.task02.utils.constant.Constant;
import com.ntukhpi.otp.vydrya.task02.view.extractor.Extractor;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

public class CalcExtractor implements Extractor<Calc, Activity> {
	private static final String TAG = CalcExtractor.class.getName();

	@Override
	public Calc extract(Activity t) {
		Log.d(TAG, "extract start");
		final Intent intent = t.getIntent();
		final Calc calc = (Calc) intent.getSerializableExtra(Constant.CALC);
		Log.d(TAG, "extract done");
		return calc;
	}
}
