package com.ntukhpi.otp.vydrya.task02.presenter.calc;

import com.ntukhpi.otp.vydrya.task02.model.CalcModel;
import com.ntukhpi.otp.vydrya.task02.model.entity.Calc;
import com.ntukhpi.otp.vydrya.task02.model.impl.CalcModelImpl;
import com.ntukhpi.otp.vydrya.task02.view.activity.calc.CalcViewLayer;

import android.util.Log;

public class CalcPresenterLayerImpl implements CalcPresenterLayer {
	private static final String TAG = CalcPresenterLayerImpl.class.getName();
	private CalcModel calcService;
	private CalcViewLayer view;
	
	public CalcPresenterLayerImpl(CalcViewLayer view) {
		calcService = new CalcModelImpl();
		this.view = view;
	}
	
	@Override
	public void calculation(Calc calc) {
		Log.d(TAG, "calculation start");
		final Calc resCalc = calcService.calculation(calc);
		view.returnToBackActivity(resCalc);
		Log.d(TAG, "calculation done");
	}
}
