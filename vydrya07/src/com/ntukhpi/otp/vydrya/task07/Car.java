package com.ntukhpi.otp.vydrya.task07;

import java.io.Serializable;

class Car implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idCar;
	private String name;
	private String owner;

	public Car() {
	}

	/**
	 * @return the idCar
	 */
	protected Long getIdCar() {
		return idCar;
	}

	/**
	 * @param idCar
	 *            the idCar to set
	 */
	protected void setIdCar(Long idCar) {
		this.idCar = idCar;
	}

	/**
	 * @return the name
	 */
	protected String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	protected void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the owner
	 */
	protected String getOwner() {
		return owner;
	}

	/**
	 * @param owner
	 *            the owner to set
	 */
	protected void setOwner(String owner) {
		this.owner = owner;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCar == null) ? 0 : idCar.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Car)) {
			return false;
		}
		Car other = (Car) obj;
		if (idCar == null) {
			if (other.idCar != null) {
				return false;
			}
		} else if (!idCar.equals(other.idCar)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (owner == null) {
			if (other.owner != null) {
				return false;
			}
		} else if (!owner.equals(other.owner)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Car [");
		if (idCar != null) {
			builder.append("idCar=");
			builder.append(idCar);
			builder.append(", ");
		}
		if (name != null) {
			builder.append("name=");
			builder.append(name);
			builder.append(", ");
		}
		if (owner != null) {
			builder.append("owner=");
			builder.append(owner);
		}
		builder.append("]");
		return builder.toString();
	}

}
