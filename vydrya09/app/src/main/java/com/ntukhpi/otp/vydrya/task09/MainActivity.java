package com.ntukhpi.otp.vydrya.task09;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.TimeZone;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getName();
    private static final String TIME_ZONE = "time-zone";
    private BroadcastReceiverIml broadcast = new BroadcastReceiverIml();
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate start");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.result);
        registerReceiver(broadcast, new IntentFilter(Intent.ACTION_TIMEZONE_CHANGED));
        Log.d(TAG, "onCreate done");
    }


    private class BroadcastReceiverIml extends BroadcastReceiver {
        private final String TAG = BroadcastReceiverIml.class.getName();

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(this.TAG, "onReceive start");
            final String timeZone = intent.getStringExtra(TIME_ZONE);
            textView.setText(timeZone);
            Log.d(this.TAG, "onReceive done");
        }
    }

    @Override
    protected void onDestroy() {
        Log.d(this.TAG, "onDestroy start");
        super.onDestroy();
        unregisterReceiver(broadcast);
        Log.d(this.TAG, "onDestroy done");
    }
}
