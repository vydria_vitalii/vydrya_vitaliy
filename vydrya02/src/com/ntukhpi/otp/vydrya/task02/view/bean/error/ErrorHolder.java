package com.ntukhpi.otp.vydrya.task02.view.bean.error;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ErrorHolder<E, T> implements Serializable {
	private static final long serialVersionUID = 3765233738948591956L;
	private final Map<E, T> errors = new HashMap<>();

	/**
	 * Adds the error.
	 *
	 * @param errorKey
	 *            the error key.
	 */
	public void add(E errorKey, T errorValue) {
		errors.put(errorKey, errorValue);
	}

	/**
	 * Adds all errors.
	 *
	 * @param errorHolder
	 *            error holder
	 */
	public void addAll(ErrorHolder<E, T> errorHolder) {
		errors.putAll(errorHolder.getErrors());
	}

	/**
	 * Checks if is empty.
	 *
	 * @return true, if is empty
	 */
	public boolean isEmpty() {
		return errors.isEmpty();
	}

	/**
	 * Gets the errors.
	 *
	 * @return the errors
	 */
	public Map<E, T> getErrors() {
		return errors;
	}

	/**
	 * Clears error holder.
	 */
	public void clear() {
		errors.clear();
	}
}
