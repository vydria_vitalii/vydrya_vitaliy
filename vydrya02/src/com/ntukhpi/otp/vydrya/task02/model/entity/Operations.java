package com.ntukhpi.otp.vydrya.task02.model.entity;

public enum Operations {
	ADDITION("+"), MULTIPLICATION("*");
	private final String value;

	private Operations(final String value) {
		this.value = value;
	}

	public String value() {
		return value;
	}
}
