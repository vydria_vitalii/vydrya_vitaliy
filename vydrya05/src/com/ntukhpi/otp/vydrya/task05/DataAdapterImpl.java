package com.ntukhpi.otp.vydrya.task05;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class DataAdapterImpl extends BaseDataAdapter<DataEntity> {
	private static final String TAG = DataAdapterImpl.class.getName();

	public DataAdapterImpl(final Context context, final List<DataEntity> list) {
		super(context, list);
	}

	// @Override
	// public int getCount() {
	// Log.d(TAG, "getCount start");
	// Log.d(TAG, "getCount done");
	// return objects.size();
	// }
	//
	// @Override
	// public Object getItem(final int position) {
	// Log.d(TAG, "getItem start");
	// Log.d(TAG, "getItem done");
	// return objects.get(position);
	// }
	//
	// @Override
	// public long getItemId(final int position) {
	// Log.d(TAG, "getItemId start");
	// Log.d(TAG, "getItemId done");
	// return position;
	// }

	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		Log.d(TAG, "getView start");
		View view = convertView;
		if (view == null) {
			view = getlInflater().inflate(R.layout.item, parent, false);
		}
		DataEntity entity = getData(position);
		((TextView) view.findViewById(R.id.name1)).setText(entity.getName1());
		((TextView) view.findViewById(R.id.name2)).setText(entity.getName2() + "");
		((ImageView) view.findViewById(R.id.img)).setImageResource(entity.getImg());

		// CheckBox cBox = (CheckBox) view.findViewById(R.id.cbBox);
		// cBox.setOnCheckedChangeListener(new
		// CompoundButton.OnCheckedChangeListener() {
		//
		// @Override
		// public void onCheckedChanged(CompoundButton buttonView, boolean
		// isChecked) {
		// getData((Integer) buttonView.getTag()).setSelect(isChecked);
		// }
		// });
		// cBox.setTag(position);
		// cBox.setChecked(entity.isSelect());
		Log.d(TAG, "getView done");
		return view;
	}

	// public DataEntity getData(final int position) {
	// Log.d(TAG, "getData start");
	// DataEntity entity = (DataEntity) getItem(position);
	// Log.i(TAG, "entity: " + entity);
	// Log.d(TAG, "getData done");
	// return entity;
	// }

	// public List<DataEntity> getSelectData() {
	// Log.d(TAG, "getSelectData start");
	// List<DataEntity> list = new ArrayList<DataEntity>();
	// for (DataEntity data : objects) {
	// if (data.isSelect()) {
	// list.add(data);
	// }
	// }
	// Log.i(TAG, "result select: " + list);
	// Log.d(TAG, "getSelectData done");
	// return list;
	// }

	// public void remove(final int position) {
	// Log.d(TAG, "remove start");
	// objects.remove(position);
	// notifyDataSetChanged();
	// Log.d(TAG, "remove done");
	// }
	//
	//
	// @Override
	// public void add(final DataEntity obj) {
	// Log.d(TAG, "add start");
	// this.objects.add(obj);
	// notifyDataSetChanged();
	// Log.d(TAG, "add done");
	// }
	//
	// @Override
	// public void sort(final Comparator<DataEntity> comparator) {
	// Log.d(TAG, "sort start");
	// Collections.sort(this.objects, comparator);
	// this.notifyDataSetChanged();
	// Log.d(TAG, "sort done");
	// }
}
