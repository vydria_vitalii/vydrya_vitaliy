package com.ntukhpi.otp.vydrya.task02.presenter.action;

import java.io.Serializable;

import com.ntukhpi.otp.vydrya.task02.view.ViewLayer;

import android.util.Log;

public abstract class BaseAction<E extends Serializable, V extends ViewLayer> implements Action<V> {
	private static final String TAG = BaseAction.class.getName();
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ntukhpi.otp.vydrya.task02.action.Action#execute(android.app.Activity)
	 */
	@Override
	public void execute(V view) {
		Log.d(TAG, "execute satrt");
		E bean = getBean(view);
		execute(view, bean);
		Log.d(TAG, "execute done");
	}

	protected abstract void execute(V view, E e);
	protected abstract E getBean(V view);
}
