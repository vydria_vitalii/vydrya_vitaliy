package com.ntukhpi.otp.vydrya.task02.view.bean;

import java.io.Serializable;

public class CalcBean implements Serializable {
	private static final long serialVersionUID = -8952053511071262984L;
	private String paramOne;
	private String operation;
	private String paramTwo;

	public CalcBean() {
	}

	/**
	 * @return the paramOne
	 */
	public String getParamOne() {
		return paramOne;
	}

	/**
	 * @param paramOne
	 *            the paramOne to set
	 */
	public void setParamOne(String paramOne) {
		this.paramOne = paramOne;
	}

	/**
	 * @return the operation
	 */
	public String getOperation() {
		return operation;
	}

	/**
	 * @param operation
	 *            the operation to set
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}

	/**
	 * @return the paramTwo
	 */
	public String getParamTwo() {
		return paramTwo;
	}

	/**
	 * @param paramTwo
	 *            the paramTwo to set
	 */
	public void setParamTwo(String paramTwo) {
		this.paramTwo = paramTwo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((operation == null) ? 0 : operation.hashCode());
		result = prime * result + ((paramOne == null) ? 0 : paramOne.hashCode());
		result = prime * result + ((paramTwo == null) ? 0 : paramTwo.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof CalcBean)) {
			return false;
		}
		CalcBean other = (CalcBean) obj;
		if (operation == null) {
			if (other.operation != null) {
				return false;
			}
		} else if (!operation.equals(other.operation)) {
			return false;
		}
		if (paramOne == null) {
			if (other.paramOne != null) {
				return false;
			}
		} else if (!paramOne.equals(other.paramOne)) {
			return false;
		}
		if (paramTwo == null) {
			if (other.paramTwo != null) {
				return false;
			}
		} else if (!paramTwo.equals(other.paramTwo)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CalcBean [");
		if (paramOne != null) {
			builder.append("paramOne=");
			builder.append(paramOne);
			builder.append(", ");
		}
		if (operation != null) {
			builder.append("operation=");
			builder.append(operation);
			builder.append(", ");
		}
		if (paramTwo != null) {
			builder.append("paramTwo=");
			builder.append(paramTwo);
		}
		builder.append("]");
		return builder.toString();
	}

}
