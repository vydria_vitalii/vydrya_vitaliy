package com.ntukhpi.otp.vydrya.task02.view.activity.calc;

import com.ntukhpi.otp.vydrya.task02.model.entity.Calc;
import com.ntukhpi.otp.vydrya.task02.presenter.calc.CalcPresenterLayer;
import com.ntukhpi.otp.vydrya.task02.presenter.calc.CalcPresenterLayerImpl;
import com.ntukhpi.otp.vydrya.task02.utils.constant.Constant;
import com.ntukhpi.otp.vydrya.task02.view.AbstractViewLayer;
import com.ntukhpi.otp.vydrya.task02.view.activity.calc.extractor.CalcExtractor;
import com.ntukhpi.otp.vydrya.task02.view.extractor.Extractor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class CalcActivity extends AbstractViewLayer implements CalcViewLayer {
	private static final String TAG = CalcActivity.class.getName();
	private CalcPresenterLayer presenter;
	private Extractor<Calc, Activity> extractor;

	public CalcActivity() {
		presenter = new CalcPresenterLayerImpl(this);
		extractor = new CalcExtractor();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate start");
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate done");
	}

	@Override
	protected Activity getActivity() {
		Log.d(TAG, "getActivity start");
		Log.d(TAG, "getActivity done");
		return this;
	}

	@Override
	protected void onResume() {
		Log.d(TAG, "onResume start");
		super.onResume();
		final Calc calc = extractor.extract(this);
		presenter.calculation(calc);
		Log.d(TAG, "onResume done");
	}

	@Override
	public void returnToBackActivity(final Calc calc) {
		Log.d(TAG, "returnToBack start");
		final Intent resIntent = new Intent();
		resIntent.putExtra(Constant.CALC, calc);
		setResult(RESULT_OK, resIntent);
		finish();
		Log.d(TAG, "returnToBack done");
	}
}
