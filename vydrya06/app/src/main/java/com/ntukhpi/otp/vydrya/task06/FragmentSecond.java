package com.ntukhpi.otp.vydrya.task06;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;


public class FragmentSecond extends Fragment {
    private static final String MULTIPLICATION = "*";
    private static final String ADDITION = "+";
    private static final String TAG = FragmentSecond.class.getName();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView start");
        Log.d(TAG, "onCreateView done");
        return inflater.inflate(R.layout.fragment_second, container, false);
    }


    @Override
    public void onStart() {
        Log.d(TAG, "onCreateView start");
        super.onStart();
        Button button = (Button) getActivity().findViewById(R.id.button_calc);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText paramOneView = (EditText) getActivity().findViewById(R.id.param_one);
                final EditText paramTwoView = (EditText) getActivity().findViewById(R.id.param_two);
                final Spinner operationView = (Spinner) getActivity().findViewById(R.id.operation);
                final TextView resultView = (TextView) getActivity().findViewById(R.id.result);

                final double firstParam = Double.parseDouble(paramOneView.getText().toString());
                final double secondParam = Double.parseDouble(paramTwoView.getText().toString());

                double res = 0.0;
                switch (operationView.getSelectedItem().toString()) {
                    case ADDITION:
                        res = firstParam + secondParam;
                        break;
                    case MULTIPLICATION:
                        res = firstParam * secondParam;
                        break;
                    default:
                        throw new UnsupportedOperationException("error operation");
                }

                resultView.setText(String.valueOf(res));
            }
        });
        Log.d(TAG, "onCreateView done");
    }
}
