package com.ntukhpi.otp.vydrya.task02.presenter.action;

import java.io.Serializable;

import com.ntukhpi.otp.vydrya.task02.presenter.validate.Validate;
import com.ntukhpi.otp.vydrya.task02.view.ViewLayer;
import com.ntukhpi.otp.vydrya.task02.view.bean.error.ErrorHolder;

import android.util.Log;

public abstract class AbstractAction<E extends Serializable, V extends ViewLayer> extends BaseAction<E, V>
		implements ValidationAction<E, V>, Validate<ErrorHolder<Integer, String>, E> {
	private static final String TAG = AbstractAction.class.getName();
	private boolean validating;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ntukhpi.otp.vydrya.task02.action.impl.BaseAction#execute(com.ntukhpi.
	 * otp.vydrya.task02.presenter.PresenterLayer, java.io.Serializable)
	 */
	@Override
	protected  void execute(V view, E e) {
		Log.d(TAG, "execute start");
		ErrorHolder<Integer, String> error = validate(e);
		validating = error.isEmpty();
		Log.i(TAG, "validating: " + validating + " error: " + error);
		if (validating) {
			onValidationSucceeded(view, e);
		} else {
			onValidationFailed(error, view);
		}
		Log.d(TAG, "execute done");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ntukhpi.otp.vydrya.task02.action.ValidationAction#isValidating()
	 */
	@Override
	public boolean isValidating() {
		Log.d(TAG, "isValidating start");
		Log.d(TAG, "isValidating done");
		return validating;
	}
}