package com.ntukhpi.otp.vydrya.task02.presenter.action;

import com.ntukhpi.otp.vydrya.task02.view.ViewLayer;

public interface Action<V extends ViewLayer> {
	void execute(V view);
}