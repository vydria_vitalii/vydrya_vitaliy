package com.ntukhpi.otp.vydrya.task02.exception;

public class AppExceptions extends RuntimeException {
	private static final long serialVersionUID = 625263993845215774L;

	public AppExceptions() {
		super();
	}

	public AppExceptions(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

	public AppExceptions(String detailMessage) {
		super(detailMessage);
	}

	public AppExceptions(Throwable throwable) {
		super(throwable);
	}

}
