package com.ntukhpi.otp.vydrya.task02.presenter.calc;

import com.ntukhpi.otp.vydrya.task02.model.entity.Calc;
import com.ntukhpi.otp.vydrya.task02.presenter.PresenterLayer;

public interface CalcPresenterLayer extends PresenterLayer {
	void calculation(Calc calc);
}
