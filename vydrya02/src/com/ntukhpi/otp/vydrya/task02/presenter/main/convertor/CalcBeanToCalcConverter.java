package com.ntukhpi.otp.vydrya.task02.presenter.main.convertor;

import com.ntukhpi.otp.vydrya.task02.model.entity.Calc;
import com.ntukhpi.otp.vydrya.task02.model.entity.Operations;
import com.ntukhpi.otp.vydrya.task02.presenter.convertor.Convertor;
import com.ntukhpi.otp.vydrya.task02.view.bean.CalcBean;

public class CalcBeanToCalcConverter implements Convertor<CalcBean, Calc> {
	private static final String MULTIPLICATION = "*";
	private static final String ADDITION = "+";

	@Override
	public Calc convert(CalcBean e) {
		Calc calc = new Calc();
		calc.setParamOne(Double.valueOf(e.getParamOne()));
		calc.setParamTwo(Double.valueOf(e.getParamTwo()));
		Operations operations;
		switch (e.getOperation()) {
		case MULTIPLICATION:
			operations = Operations.MULTIPLICATION;
			break;
		case ADDITION:
			operations = Operations.ADDITION;
			break;
		default:
			throw new IllegalArgumentException("non operations");
		}
		calc.setOperations(operations);
		return calc;
	}
}
