package com.ntukhpi.otp.vydrya.task06;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentFirs extends android.support.v4.app.Fragment {
    private static final String TAG = FragmentFirs.class.getName();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView start");
        Log.d(TAG, "onCreateView done");
        return inflater.inflate(R.layout.fragment_first, container, false);
    }
}
