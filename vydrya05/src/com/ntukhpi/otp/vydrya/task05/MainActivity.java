package com.ntukhpi.otp.vydrya.task05;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.ntukhpi.otp.vydrya.task05.R;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class MainActivity extends Activity {
	private static final String TAG = MainActivity.class.getName();
	private static final int LEN_STR = 5;
	private static final int COUNT_LIST = 10;

	private List<DataEntity> listData = new ArrayList<DataEntity>();
	private RandomString randomString;
	private DataAdapter<DataEntity> adapter;
	private ListView lvMain;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		Log.d(TAG, "onCreate start");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		randomString = new RandomString(LEN_STR);

		fillListData(listData, randomString, COUNT_LIST);
		adapter = new DataAdapterImpl(this, listData);

		lvMain = (ListView) findViewById(R.id.lvMain);
		lvMain.setAdapter(adapter);

		Button remove = (Button) findViewById(R.id.button_remove);
		remove.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				final DataEntity entity = generateData(randomString);
				adapter.remove(0);
				adapter.add(entity);
			}
		});

		fillListData(listData, randomString, COUNT_LIST);
		Log.d(TAG, "onCreate done");
	}

	private void fillListData(final List<DataEntity> list, final RandomString rand, final int lenList) {
		Log.d(TAG, "fillData start");
		for (int i = 0; i < lenList; i++) {
			list.add(generateData(rand));
		}
		Log.d(TAG, "fillData done");
	}

	private DataEntity generateData(final RandomString rand) {
		Log.d(TAG, "generateData start");
		DataEntity entity = new DataEntity();
		entity.setName1(rand.nextString());
		entity.setName2(rand.nextString());
		entity.setImg(R.drawable.ic_launcher);
		Log.i(TAG, "generateData data: " + entity);
		Log.d(TAG, "generateData done");
		return entity;
	}

	public void scrollTop(final View v) {
		Log.d(TAG, "scroll start");
		lvMain.smoothScrollToPosition(0);
		Log.d(TAG, "scroll start");
	}

	public void scrollDown(final View v) {
		Log.d(TAG, "scrollDown start");
		lvMain.smoothScrollToPosition(adapter.getCount());
		Log.d(TAG, "scrollDown start");
	}

	public void sort1(final View v) {
		Log.d(TAG, "sort1 start");
		adapter.sort(new ComparatorSort1());
		Log.d(TAG, "sort1 done");
	}

	public void sort2(final View v) {
		Log.d(TAG, "sort2 start");
		adapter.sort(new ComparatorSort2());
		Log.d(TAG, "sort2 done");
	}

	private static class ComparatorSort1 implements Comparator<DataEntity> {
		protected Character getLastChar(String str) {
			return str.charAt(str.length() - 1);
		}

		@Override
		public int compare(DataEntity lhs, DataEntity rhs) {
			final Character letf = getLastChar(lhs.getName2());
			final Character right = getLastChar(rhs.getName2());
			return letf.compareTo(right);
		}

	}

	private static class ComparatorSort2 extends ComparatorSort1 {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.ntukhpi.otp.vydrya.task05.MainActivity.ComparatorSort1#compare(
		 * com.ntukhpi.otp.vydrya.task05.DataEntity,
		 * com.ntukhpi.otp.vydrya.task05.DataEntity)
		 */
		@Override
		public int compare(DataEntity lhs, DataEntity rhs) {
			return super.compare(lhs, rhs) * -1;
		}
	}
}
