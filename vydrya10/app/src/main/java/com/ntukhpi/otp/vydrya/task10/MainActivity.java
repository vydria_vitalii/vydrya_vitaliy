package com.ntukhpi.otp.vydrya.task10;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class MainActivity extends Activity {
    private static final String TAG = MainActivity.class.getName();
    private final String URL_PARH = "https://www.google.com.ua";
    private static final String METHOD = "POST";
    private static final int TIME_WAIT_SECONDS = 10;

    private class RunnableUI implements Runnable {
        private final String TAG = this.getClass().getName();
        private static final String RESULT_OK = "«HTML сторінка";
        private static final String RESULT_NO_OK = "«не HTML сторінка";
        private boolean result;

        public RunnableUI() {
            this(false);
        }

        public RunnableUI(final boolean result) {
            this.result = result;
        }

        @Override
        public void run() {
            Log.d(TAG, "run start");
            ((TextView) findViewById(R.id.response)).setText(result ? RESULT_OK : RESULT_NO_OK);
            Log.d(TAG, "run done");
        }
    }

    private final Runnable background = new Runnable() {
        private final String TAG = this.getClass().getName();
        private final String PATTERN_DOCTYPE = "<\\s*![dD][oO][Cc][Tt][yY][pP][eE]\\s* [hH][Tt][Mm][Ll](\\s+.*|\\s+)>";
        private final String PATTERN_HTML = "<\\s*[hH][tT][Mm][lL](\\s*|\\s+.+)>";

        @Override
        public void run() {
            Log.d(TAG, "run start");
            HttpURLConnection conn = null;
            try {
                final java.net.URL url = new java.net.URL(URL_PARH);
                conn = (HttpURLConnection) url.openConnection();

                conn.setRequestMethod(METHOD);
                conn.setRequestProperty("User-Agent", "Mozilla/5.0");
                conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

                conn.setConnectTimeout((int) TimeUnit.SECONDS.toMillis(TIME_WAIT_SECONDS));
                conn.setReadTimeout((int) TimeUnit.SECONDS.toMillis(TIME_WAIT_SECONDS));
                conn.connect();
				if(connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
					boolean result = false;
					BufferedReader reader = null;
					try {
						reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
						StringBuilder line = new StringBuilder();

						boolean flagDoctype = false;
						do {
							line.append(reader.readLine());
							if (Objects.equals("null", line.toString())) {
								break;
							} else {
								if (!flagDoctype && Pattern.matches(PATTERN_DOCTYPE, line.toString())) {
									flagDoctype = true;
								}
								result = Pattern.matches(PATTERN_HTML, line.toString());
								line.setLength(0);
							}
						} while (!result);
					} finally {
						closeResource(reader);
					}
					MainActivity.this.runOnUiThread(new RunnableUI(result));
				}
				else {
					Log.w(TAG,"Bad result http: " + connection.getResponseCode());
				}

            } catch (MalformedURLException e) {
                Log.wtf(TAG, "error url", e);
                throw new RuntimeException("error url", e);
            } catch (IOException e) {
                Log.wtf(TAG, "problem connectiuon", e);
                throw new RuntimeException("problem connectiuon", e);
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }
            Log.d(TAG, "onResume done");
        }
    };


    private void closeResource(final AutoCloseable resource) {
        Log.d(TAG, "closeResource start");
        if (resource != null) {
            try {
                resource.close();
            } catch (Exception e) {
                Log.w(TAG, "Error close resource");
            }
        }
        Log.d(TAG, "closeResource done");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate start");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate done");
    }


    @Override
    protected void onResume() {
        Log.d(TAG, "onResume start");
        super.onResume();
        Thread thread = new Thread(background);
        thread.start();

    }
}