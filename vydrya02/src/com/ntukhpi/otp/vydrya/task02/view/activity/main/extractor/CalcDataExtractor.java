package com.ntukhpi.otp.vydrya.task02.view.activity.main.extractor;

import com.ntukhpi.otp.vydrya.task02.model.entity.Calc;
import com.ntukhpi.otp.vydrya.task02.utils.constant.Constant;
import com.ntukhpi.otp.vydrya.task02.view.extractor.Extractor;

import android.content.Intent;
import android.util.Log;

public class CalcDataExtractor implements Extractor<Calc, Intent> {
	private static final String TAG = CalcBeanExtractor.class.getName();

	@Override
	public Calc extract(final Intent data) {
		Log.d(TAG, "extract start");
		final Calc res = (Calc) data.getSerializableExtra(Constant.CALC);
		Log.d(TAG, "extract done");
		return res;
	}

}
