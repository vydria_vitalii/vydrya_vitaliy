package com.ntukhpi.otp.vydrya.task02.view.activity.main;

import com.ntukhpi.otp.vydrya.task02.R;
import com.ntukhpi.otp.vydrya.task02.model.entity.Calc;
import com.ntukhpi.otp.vydrya.task02.presenter.main.MainPresenterLayer;
import com.ntukhpi.otp.vydrya.task02.presenter.main.MainPresenterLayerImpl;
import com.ntukhpi.otp.vydrya.task02.utils.constant.Constant;
import com.ntukhpi.otp.vydrya.task02.view.AbstractViewLayer;
import com.ntukhpi.otp.vydrya.task02.view.activity.calc.CalcActivity;
import com.ntukhpi.otp.vydrya.task02.view.activity.main.extractor.CalcBeanExtractor;
import com.ntukhpi.otp.vydrya.task02.view.activity.main.extractor.CalcDataExtractor;
import com.ntukhpi.otp.vydrya.task02.view.bean.CalcBean;
import com.ntukhpi.otp.vydrya.task02.view.extractor.Extractor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AbstractViewLayer implements MainViewLayer {
	private static final String TAG = MainActivity.class.getName();
	public static final int CALC_REQUEST_CODE = 1;

	private EditText paramOne;
	private Spinner operation;
	private EditText paramTwo;
	private TextView result;

	private MainPresenterLayer presenter;
	private Extractor<CalcBean, MainActivity> calcExtr;
	private Extractor<Calc, Intent> dataResExtr;

	public MainActivity() {
		presenter = new MainPresenterLayerImpl(this);
		calcExtr = new CalcBeanExtractor();
		dataResExtr = new CalcDataExtractor();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate start");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		paramOne = (EditText) findViewById(R.id.param_one);
		operation = (Spinner) findViewById(R.id.operation);
		paramTwo = (EditText) findViewById(R.id.param_two);
		result = (TextView) findViewById(R.id.result);

		Button button = (Button) findViewById(R.id.button_calc);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d(TAG, "onCreate start");
				presenter.onCalcButtonClick();
				Log.d(TAG, "onCreate start");
			}
		});
		Log.d(TAG, "onCreate done");
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG, "onActivityResult start");
		if (CALC_REQUEST_CODE == requestCode) {
			if (RESULT_OK == resultCode) {
				final Calc calc = dataResExtr.extract(data);
				presenter.onResultCalc(calc);
			}
		}
		Log.d(TAG, "onActivityResult done");
	}

	@Override
	public CalcBean getData() {
		Log.d(TAG, "getData start");
		Log.d(TAG, "getData done");
		return calcExtr.extract(this);
	}

	@Override
	protected Activity getActivity() {
		Log.d(TAG, "getActivity start");
		Log.d(TAG, "getActivity done");
		return this;
	}

	/**
	 * @return the paramOne
	 */
	public EditText getParamOne() {
		return paramOne;
	}

	/**
	 * @param paramOne
	 *            the paramOne to set
	 */
	public void setParamOne(EditText paramOne) {
		this.paramOne = paramOne;
	}

	/**
	 * @return the operations
	 */
	public Spinner getOperation() {
		return operation;
	}

	/**
	 * @param operations
	 *            the operations to set
	 */
	public void setOperations(Spinner operations) {
		this.operation = operations;
	}

	/**
	 * @return the paramTwo
	 */
	public EditText getParamTwo() {
		return paramTwo;
	}

	/**
	 * @param paramTwo
	 *            the paramTwo to set
	 */
	public void setParamTwo(EditText paramTwo) {
		this.paramTwo = paramTwo;
	}

	@Override
	public void calculation(Calc calc) {
		Log.d(TAG, "calculation start");
		Intent intent = new Intent(this, CalcActivity.class);
		intent.putExtra(Constant.CALC, calc);
		startActivityForResult(intent, CALC_REQUEST_CODE);
		Log.d(TAG, "calculation done");
	}

	@Override
	public void showResultCalc(final Calc calc) {
		Log.d(TAG, "showResultCalc start");
		result.setText(String.valueOf(calc.getReulst()));
		Log.d(TAG, "showResultCalc done");
	}
}
