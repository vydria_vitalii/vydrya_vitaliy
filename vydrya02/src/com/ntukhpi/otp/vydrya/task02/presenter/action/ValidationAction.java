package com.ntukhpi.otp.vydrya.task02.presenter.action;

import java.io.Serializable;

import com.ntukhpi.otp.vydrya.task02.view.ViewLayer;
import com.ntukhpi.otp.vydrya.task02.view.bean.error.ErrorHolder;

public interface ValidationAction<E extends Serializable, V extends ViewLayer> {

	void onValidationFailed(ErrorHolder<Integer, String> errorHolder, V view);

	void onValidationSucceeded(V view, E e);

	boolean isValidating();
}
