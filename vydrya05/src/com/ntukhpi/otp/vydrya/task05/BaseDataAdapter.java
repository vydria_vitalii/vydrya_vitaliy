package com.ntukhpi.otp.vydrya.task05;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;

public abstract class BaseDataAdapter<T> extends BaseAdapter implements DataAdapter<T> {
	private static final String TAG = BaseAdapter.class.getName();
	private final Context context;
	private final LayoutInflater lInflater;
	private final List<T> objects;

	public BaseDataAdapter(final Context context, final List<T> list) {
		this.context = context;
		objects = new ArrayList<T>(list);
		lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	/**
	 * @return the context
	 */
	protected Context getContext() {
		Log.d(TAG, "getContext start");
		Log.d(TAG, "getContext done");
		return context;
	}

	/**
	 * @return the lInflater
	 */
	protected LayoutInflater getlInflater() {
		Log.d(TAG, "LayoutInflater start");
		Log.d(TAG, "LayoutInflater done");
		return lInflater;
	}

	/**
	 * @return the objects
	 */
	protected List<T> getObjects() {
		Log.d(TAG, "getObjects start");
		Log.d(TAG, "getObjects done");
		return objects;
	}

	@Override
	public int getCount() {
		Log.d(TAG, "getCount start");
		Log.d(TAG, "getCount done");
		return objects.size();
	}

	@Override
	public Object getItem(final int position) {
		Log.d(TAG, "getItem start");
		Log.d(TAG, "getItem done");
		return objects.get(position);
	}

	@Override
	public long getItemId(final int position) {
		Log.d(TAG, "getItemId start");
		Log.d(TAG, "getItemId done");
		return position;
	}

	public void remove(final int position) {
		Log.d(TAG, "remove start");
		objects.remove(position);
		notifyDataSetChanged();
		Log.d(TAG, "remove done");
	}

	@Override
	public void add(final T obj) {
		Log.d(TAG, "add start");
		this.objects.add(obj);
		notifyDataSetChanged();
		Log.d(TAG, "add done");
	}

	@Override
	public void sort(final Comparator<T> comparator) {
		Log.d(TAG, "sort start");
		Collections.sort(this.objects, comparator);
		this.notifyDataSetChanged();
		Log.d(TAG, "sort done");
	}

	public DataEntity getData(final int position) {
		Log.d(TAG, "getData start");
		DataEntity entity = (DataEntity) getItem(position);
		Log.i(TAG, "entity: " + entity);
		Log.d(TAG, "getData done");
		return entity;
	}
}
