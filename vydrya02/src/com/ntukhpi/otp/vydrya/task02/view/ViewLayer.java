package com.ntukhpi.otp.vydrya.task02.view;

import com.ntukhpi.otp.vydrya.task02.view.bean.error.ErrorMessage;

public interface ViewLayer {
	void showError(ErrorMessage error);
}