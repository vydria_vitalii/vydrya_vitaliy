package com.ntukhpi.otp.vydrya.task07;

public final class CarUtils {
	private CarUtils() {
		throw new UnsupportedOperationException("non instance this class");
	}

	// name table
	public static final String NAME_TABLE = "car";

	// name fields
	public static final String ID_CAR = "id_car";
	public static final String CAR_NAME = "name";
	public static final String OWNER = "owner";
}
