package com.ntukhpi.otp.vydrya.task02.view.activity.main;

import com.ntukhpi.otp.vydrya.task02.model.entity.Calc;
import com.ntukhpi.otp.vydrya.task02.view.ViewLayer;
import com.ntukhpi.otp.vydrya.task02.view.bean.CalcBean;
import com.ntukhpi.otp.vydrya.task02.view.bean.error.ErrorHolder;

public interface MainViewLayer extends ViewLayer {
	CalcBean getData();

	void calculation(Calc calc);

	void showInvalidData(ErrorHolder<Integer, String> error);

	void showResultCalc(Calc calc);
}