package com.ntukhpi.otp.vydrya.task02.presenter.validate;

public interface Validate<V, E> {
	V validate(E e);
}
