package com.ntukhpi.otp.vydrya.task07;

import java.io.Closeable;
import java.io.IOException;

import com.example.vydrya07.R;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity {
	private static final String TAG = MainActivity.class.getName();
	private DBManager dbManager;
	private SQLiteDatabase db;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		dbManager = new DBManager(this);
		db = dbManager.getWritableDatabase();

		Cursor cursor = db.query(CarUtils.NAME_TABLE, null, null, null, null, null, null);
		Log.i(TAG, "-----" + CarUtils.NAME_TABLE + "-----\n");
		showLogDB(cursor);
		closeResource(cursor);

		cursor = db.query(CarUtils.NAME_TABLE, null, CarUtils.ID_CAR + " = ?", new String[] { "4" }, null, null, null);
		Log.i(TAG, "search result\n");
		showLogDB(cursor);
		closeResource(cursor);

		Log.i(TAG, "delete records: " + db.delete(CarUtils.NAME_TABLE, null, null));
	}

	private void showLogDB(final Cursor cursor) {
		Log.d(TAG, "showLog start");
		if (cursor != null) {
			StringBuilder builder = new StringBuilder();
			while (cursor.moveToNext()) {
				for (final String str : cursor.getColumnNames()) {
					builder.append(str + " = " + cursor.getString(cursor.getColumnIndex(str)) + "; ");
				}
				builder.append("\n");
			}
			builder.deleteCharAt(builder.length() - 1);
			Log.i(TAG, builder.toString());
		}
		Log.d(TAG, "showLog done");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onDestroy()
	 */
	@Override
	protected void onDestroy() {
		Log.d(TAG, "onDestroy start");
		super.onDestroy();
		closeResource(db);
		closeResource(dbManager);
		Log.d(TAG, "onDestroy done");
	}

	private void closeResource(final Closeable res) {
		Log.d(TAG, "closeResource start");
		if (res != null) {
			try {
				res.close();
			} catch (IOException e) {
				Log.w(TAG, "Error close resouce", e);
			}
		}
		Log.d(TAG, "closeResource done");
	}
}
