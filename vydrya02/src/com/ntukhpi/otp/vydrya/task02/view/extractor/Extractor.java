package com.ntukhpi.otp.vydrya.task02.view.extractor;


public interface Extractor<E, T> {
	E extract(T t);
}
