package com.ntukhpi.otp.vydrya.task02.presenter.action;

import java.io.Serializable;

import com.ntukhpi.otp.vydrya.task02.model.entity.Entity;
import com.ntukhpi.otp.vydrya.task02.presenter.convertor.Convertor;
import com.ntukhpi.otp.vydrya.task02.view.ViewLayer;

import android.util.Log;

public abstract class ActionSupport<B extends Serializable, E extends Entity, V extends ViewLayer>
		extends AbstractAction<B, V> implements Convertor<B, E>, ConvertorAction<E, V> {
	private static final String TAG = ActionSupport.class.getName();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ntukhpi.otp.vydrya.task02.action.ValidationAction#
	 * onValidationSucceeded(com.ntukhpi.otp.vydrya.task02.presenter.
	 * PresenterLayer, java.io.Serializable)
	 */
	@Override
	public void onValidationSucceeded(final V view, final B bean) {
		Log.d(TAG, "onValidationSucceeded start");
		this.execute(view, convert(bean));
		Log.d(TAG, "onValidationSucceeded done");
	}
}
