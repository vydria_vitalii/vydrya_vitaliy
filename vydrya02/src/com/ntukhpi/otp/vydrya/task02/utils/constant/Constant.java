package com.ntukhpi.otp.vydrya.task02.utils.constant;

public final class Constant {
	private Constant() {
		throw new UnsupportedOperationException("non instance Constant");
	}
	
	public static final String CALC = "calc";	
}
