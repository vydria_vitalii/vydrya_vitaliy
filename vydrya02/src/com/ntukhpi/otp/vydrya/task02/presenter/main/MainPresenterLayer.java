package com.ntukhpi.otp.vydrya.task02.presenter.main;

import com.ntukhpi.otp.vydrya.task02.model.entity.Calc;
import com.ntukhpi.otp.vydrya.task02.presenter.PresenterLayer;

public interface MainPresenterLayer extends PresenterLayer {
	void onCalcButtonClick();
	void onResultCalc(Calc calc);
}